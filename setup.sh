sudo apt-get install -y cmake g++ curl

mkdir -p ext_deps
(
    cd ext_deps
    curl -LsS https://api.github.com/repos/relic-toolkit/relic/tarball/25aafec > relic.tar.gz
    tar xzf relic.tar.gz
    mkdir relic-target
    
    (
        cd relic-target
        cmake ../relic-toolkit-relic-25aafec/
        make
        make install DESTDIR=../
    )

    # `make install` actually makes a mess
    # so we re-order the files in ext_deps
    # to have the following structure:
    # relic/include/
    # relic/lib/
    # relic/cmake

    mkdir -p relic relic/include relic/lib relic/cmake
    mv usr/local/include/relic/* relic/include/
    mv usr/local/lib/* relic/lib/
    mv usr/local/cmake/relic-config.cmake relic/cmake/

    rm -r relic.tar.gz relic-toolkit-relic-25aafec relic-target usr
)

# If we are in a vagrant VM,
# then the project directory should be at /vagrant
if [ -e /vagrant ] ; then
    rm -rf /vagrant/ext_deps
    mv ext_deps /vagrant
    ln -s /vagrant muse
fi

echo "SETUP END"
