#include "muse.hpp"

int main(){

	if (core_init() != STS_OK) {
		core_clean();
		return 1;
	}

	if (ec_param_set_any() == STS_ERR) {
		THROW(ERR_NO_CURVE);
		core_clean();
		return 0;
	}

	if (pc_param_set_any() != STS_OK) {
		THROW(ERR_NO_CURVE);
		core_clean();
		return 0;
	}

  Writer writer;
  Reader reader;
  QueryMultiplexer qm;
  IndexHost ih;

  std::string keyword = "foo";

  gt_t enc_kw;
  gt_new(enc_kw);
  writer.encrypt(enc_kw, keyword);

  g2_t auth;
  g2_new(auth);
  writer.delegate(auth, reader.pubkey);

  // We're going to need the pairing group order
  // just for the generation of xi
  // (in the future this will be handled by lower level components)
  bn_t order;
  bn_new(order);
  g2_get_ord(order);

  // Generation of blinding factor
  // (should be done by Reader,
  // but actually both the Reader and IH will do it separately
  // based on a common seed)
  bn_t xi;
  bn_new(xi);
  bn_rand_mod(xi, order);

  // IH uses the xi to prepare the encrypted index
  // and performs PSI pre-processing
  ec_t ih_side;
  ec_new(ih_side);
  ih.prepare_and_psi_preprocess(ih_side, enc_kw, xi);

  // QM receives the encrypted keyword (prepared, encoded and masked)
  // and applies its PSI key
  // obtained value should go in some hash table
  ec_t ih_side_final;
  ec_new(ih_side_final);
  qm.psi_preprocess_receive(ih_side_final, ih_side);

  // Reader creates trapdoor
  g1_t trapdoor;
  g1_new(trapdoor);
  reader.trapdoor(trapdoor, keyword, xi); 

  // QM transforms trapdoor
  gt_t transf_trapdoor;
  gt_new(transf_trapdoor);
  qm.transform(transf_trapdoor, trapdoor, auth);

  // QM sends the encrypted keyword (encoded and masked)
  // to IH for IH to apply its PSI key on it
  ec_t qm_side;
  ec_new(qm_side);
  qm.psi_query(qm_side, transf_trapdoor);

  // IH applies its PSI key
  ec_t qm_side_final;
  ec_new(qm_side);
  ih.psi_response(qm_side_final, qm_side);

  // the two final values should match
	if (ec_cmp(ih_side_final, qm_side_final) == CMP_EQ) {
		printf("[OK] match\n");
	} else {
        printf("[ERROR] no match\n");
        return 1;
	}

  std::cout << "END OF TEST PROGRAM" << std::endl;
  return 0;
}

