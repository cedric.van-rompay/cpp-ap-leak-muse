#ifndef MUSE_H
#define MUSE_H

#include <string>
#include <iostream>

extern "C"{
#include <relic.h>
}

class Writer {
  public:
    bn_t key;
    g2_t g2_key;

    Writer();

    void encrypt(gt_t enc_kw, std::string keyword);
    void delegate(g2_t authorization, g2_t reader_pubkey);
};

class Reader {
  public:
    bn_t seckey;
    g2_t pubkey;

    Reader();

    void trapdoor(g1_t trapdoor, std::string keyword, bn_t xi);
};

// XXX This class should not have to be used by other code
// so I guess it shouldn't be in the header file?
// But then I am not sure how to declare some attributes of the classes that need it
class PSIEntity {
  public:
    bn_t seckey;
    ec_t pubkey;

    PSIEntity();

    int encode_and_mask(ec_t dest, gt_t src);
    int mask(ec_t dest, ec_t p);
};

class IndexHost {
  private:
    PSIEntity psi_entity;
  public:
    IndexHost();

    void prepare_and_psi_preprocess(ec_t dest, gt_t enc_kw, bn_t xi);
    void psi_response(ec_t dest, ec_t p);
};

class QueryMultiplexer{
  private:
    PSIEntity psi_entity;
  public:
    QueryMultiplexer();

    void transform(gt_t transf_trapdoor, g1_t trapdoor, g2_t auth);
    void psi_preprocess_receive(ec_t dest, ec_t p);
    void psi_query(ec_t dest, gt_t transf_trapdoor);
};

#endif /* MUSE_H */
