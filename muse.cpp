#include "muse.hpp"

Writer::Writer(){
  bn_t order;
  bn_new(order);
  g2_get_ord(order);

  bn_new(key);
  bn_rand_mod(key, order);

  g2_new(g2_key);
  g2_mul_gen(g2_key, key);
}

void Writer::encrypt(gt_t enc_kw, std::string keyword){
  g1_t h;
  g1_new(h);

  // TODO Cryptographic hash here
  g1_map(h,
         reinterpret_cast<const unsigned char *>(keyword.c_str()),
         keyword.length());

  pc_map(enc_kw, h, g2_key);
}
void Writer::delegate(g2_t authorization, g2_t reader_pubkey){
  g2_mul(authorization, reader_pubkey, this->key);
}

Reader::Reader(){
  // we will need the order
  bn_t order;
  bn_new(order);
  g2_get_ord(order);

  // reader secret key ("rho_priv")
  bn_new(seckey);
  bn_rand_mod(seckey, order);

  // for the public key we need
  // the inverse of rho modulo the order
  bn_t k, rho_priv_inv, gcd;
  bn_new(k);
  bn_new(rho_priv_inv);
  bn_new(gcd);

  bn_gcd_ext(gcd, rho_priv_inv, k, seckey, order);
  // XXX is the order supposed to be prime?
  // if so this check is not necessary
  if(bn_cmp_dig(gcd, (dig_t)1) != CMP_EQ){
    printf("ERROR: gcd != 1\n");
    exit(1);
  }

  // reader public key ("rho_pub")
  g2_new(pubkey);
  g2_mul_gen(pubkey, rho_priv_inv);
}

void Reader::trapdoor(g1_t trapdoor, std::string keyword, bn_t xi){
  // TODO factorize (redundant with Writer::Encrypt)
  g1_t h;
  g1_new(h);

  // TODO Cryptographic hash here
  g1_map(h,
         reinterpret_cast<const unsigned char *>(keyword.c_str()),
         keyword.length());

  g1_mul(trapdoor, h, this->seckey);
  g1_mul(trapdoor, trapdoor, xi);
}

PSIEntity::PSIEntity(){
  bn_t n;
  bn_new(n);
  ec_curve_get_ord(n);

  bn_rand_mod(seckey, n);
  ec_mul_gen(pubkey, seckey);
}

int PSIEntity::encode_and_mask(ec_t dest, gt_t src){
  int prep_bin_length = gt_size_bin(src, true);
  uint8_t prep_bin[prep_bin_length];
  gt_write_bin(prep_bin, prep_bin_length, src, true);

  // XXX hardcoded buffer length
  // but using RELIC_BN_BYTES
  // was causing errors
  int x_data_length = 16;
  uint8_t x_data[x_data_length];
  memcpy( x_data, prep_bin, x_data_length );

  bn_t x;
  bn_new(x);
  bn_read_bin(x, x_data, x_data_length);

  ec_mul(dest, pubkey, x);

  return 0;
}

int PSIEntity::mask(ec_t dest, ec_t p){
  ec_mul(dest, p, seckey);

  return 0;
}

IndexHost::IndexHost(){};

void IndexHost::prepare_and_psi_preprocess(ec_t dest, gt_t enc_kw, bn_t xi){
  gt_t prepared_enc_kw;
  gt_new(prepared_enc_kw);

  // note that in GT it's considered as an exponentiation
  gt_exp(prepared_enc_kw, enc_kw, xi);

  psi_entity.encode_and_mask(dest, prepared_enc_kw);
}

void IndexHost::psi_response(ec_t dest, ec_t p){
  psi_entity.mask(dest, p);
}

QueryMultiplexer::QueryMultiplexer(){};

void QueryMultiplexer::transform(gt_t transf_trapdoor, g1_t trapdoor, g2_t auth){
  pc_map(transf_trapdoor, trapdoor, auth);
}

void QueryMultiplexer::psi_preprocess_receive(ec_t dest, ec_t p){
  psi_entity.mask(dest, p);
}

void QueryMultiplexer::psi_query(ec_t dest, gt_t transf_trapdoor){
  psi_entity.encode_and_mask(dest, transf_trapdoor);
}

