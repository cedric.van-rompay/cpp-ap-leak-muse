RELIC_INCLUDE = ext_deps/relic/include/
RELIC_LIB = ext_deps/relic/lib/
CC_OPTIONS = -I $(RELIC_INCLUDE) -L $(RELIC_LIB) -lrelic -Wl,-rpath=$(RELIC_LIB)

.PHONY: default
default: dirs bin/test
	
bin/test: test.cpp muse.cpp
	g++ -o $@ $^ $(CC_OPTIONS)

.PHONY:dirs
dirs:
	mkdir -p bin

.PHONY: test
test: bin/test 
	@echo "[make] launching test program"
	bin/test
